/*$(window).load(function () {
    $(".loader").fadeOut("slow");
});*/


$(document).ready(function () {
    var $body = $('body');
    var todayDiscounts = Number(0);
    var totalDiscounts = Number(0);
    var todayDealsPages = Number(0);
    var pageIndex = Number(0);
    var pageIndexPrev = Number(0);
    var pageIndexNext = Number(0);
    var itemsPerPage = Number(16);
    //var firstBannerIndex = Math.floor(Math.random() * 13);
    var googlePlayBannerIndex = Number(1);
    var firstBannerIndex = Number(7);
    var secondBannerIndex = Number(13);
    var checkInterval = Number(180000);
    var todayDiscountsUrl = "http://api.2for1.pro/api/salecount?saleval=50&period=day&request_source=2";
    var totalDiscountsUrl = "http://api.2for1.pro/api/totalcount?request_source=2";
    var googlePlayLink = "https://play.google.com/store/apps/details?id=pro.twoforone.app&utm_source=live_2for1&utm_medium=refferal&utm_campaign=group";
    var bannerCodeOne = '<div class="grid"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>' +
        '<!-- live241_250x250_01_internal -->' +
        '<ins class="adsbygoogle" style="display:inline-block;width:200px;height:200px" data-ad-client="ca-pub-2032573220905009" data-ad-slot="9447932435"></ins>' +
        '<script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div>';
    var bannerCodeTwo = '<div class="grid"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>' +
        '<!-- live241_250x250_02_internal -->' +
        '<ins class="adsbygoogle" style="display:inline-block;width:200px;height:200px" data-ad-client="ca-pub-2032573220905009" data-ad-slot="1924665635"></ins>' +
        '<script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div>';
    var googlePlayBannerCode = '<div class="grid" id="google-play-block">' +
        '<p class="button-google-play"><a href="' + googlePlayLink + '" rel="nofollow" target="_blank"><img alt="google-play-btn" src="img/google-play-logo.png"/></a></p>' +
        '<p>Download the Android app and well show you the best deals and discounts from US biggest online stores in real time!</p>' +
        '<p class="button-google-play"><a href="' + googlePlayLink + '" rel="nofollow" target="_blank"><img alt="google-play-btn" src="img/google-play-btn.png"/></a></p>' +
        '<p class="text-center">or use QR code and download application</p>' +
        '<p class="text-center"><img src="img/play_google_2for1.pro.png"/></p></div>';


    var getTotalDiscounts = function () {
        return $.get(totalDiscountsUrl, null, null, 'text');
    };


    var getTodayDiscounts = function () {
        return $.get(todayDiscountsUrl, null, null, 'text');
    };


    $.when(getTotalDiscounts(), getTodayDiscounts()).done(function (total, today) {
        totalDiscounts = Number(total[0]);
        todayDiscounts = Number(today[0]);
        $("#total-discounts-value").text(totalDiscounts);
        $("#today-discounts-value").text(todayDiscounts);
        todayDealsPages = Math.round(todayDiscounts / itemsPerPage);
        if (isNaN(parseInt(getParam("page")))) {
            pageIndex = Number(0);
            pageIndexNext = Number(1);
            pageIndexPrev = Number(0);
        } else {
            pageIndex = parseInt(getParam("page"));
            if (pageIndex < todayDealsPages) {
                pageIndexNext = Number(pageIndex) + 1;
                pageIndexPrev = Number(pageIndex) - 1;
            } else if (pageIndex >= todayDealsPages) {
                pageIndexNext = Number(0);
                pageIndexPrev = Number(pageIndex) - 1;
            }
        }
        if (pageIndexNext > 0) {
            $('#button-next').attr('href', '/?page=' + pageIndexNext);
        }
        if (pageIndexPrev > 0) {
            $('#button-previous').attr('href', '/?page=' + pageIndexPrev);
        }
        var $divProducts = $("#products");
        var brand, discountBadgeClass, productImages, gallery, title, picUrl, price, sale, goButton, source, itemBlock = "";
        $.getJSON("http://api.2for1.pro/api/list?disfrom=50&disto=100&Pz=" + itemsPerPage + "&Pi=" + pageIndex + "&request_source=2&sort=uid", function (json) {
            $.each(json, function (i, item) {
                if (item.Brand) {
                    brand = "<p class='brand'>" + item.Brand + "</p>";
                } else {
                    brand = "";
                }
                if (parseInt(item.Type) <= 50) {
                    discountBadgeClass = "discount-badge-50";
                } else {
                    discountBadgeClass = "discount-badge-big";
                }
                productImages = item.Images.split(",");
                gallery = "";
                for (var j = 0; j < productImages.length; j++) {
                    gallery = gallery + "<a href='" + productImages[j] + "' data-lightbox='" + item.UID + "' data-title='" + item.Brand + " " + item.Title + "'></a>";
                }
                title = "<p class='title'>" + item.Title + "<div class='" + discountBadgeClass + "'>-" + item.Type + "%</div></p>";
                picUrl = "<div class='imgholder'><a href='" + item.Picurl + "' data-lightbox='" + item.UID + "' data-title='" + item.Brand + " " + item.Title + "'><img src='" + item.Picurl + "'/></a>" + gallery + "</div>";
                price = "<p class='price'>$" + parseFloat(item.Price2).toFixed(2) + "</p>";
                sale = "<p class='sale'>$" + parseFloat(item.Price1).toFixed(2) + "</p>";
                //goButton = "<p class='button-go'><a class='button button-green' href='" + item.Url + "' rel='nofollow' target='_blank'><span>GO TO STORE!</span></a></p>";
                goButton = "<p class='button-go'><a class='btn btn-success btn-lg btn-block' href='" + item.Url + "' rel='nofollow' target='_blank'><span>GO TO STORE!</span></a></p>";
                source = "<div class='imgholder store-logo'><img src='img/" + item.Source + ".png'></div>";
                itemBlock = "<div class='grid'>" + source + picUrl + brand + title + price + sale + goButton + "</div>";
                switch (i) {
                    case googlePlayBannerIndex:
                        $divProducts.append(googlePlayBannerCode);
                        break;
                    case firstBannerIndex:
                        $divProducts.append(bannerCodeOne);
                        break;
                    case secondBannerIndex:
                        $divProducts.append(bannerCodeTwo);
                        break;
                    default:
                        $divProducts.append(itemBlock);
                        break;
                }
            });
            setTimeout(function () {
                $divProducts.BlocksIt({
                    numOfCol: 4,
                    offsetX: 8,
                    offsetY: 8
                });
            }, 1500);
        });
    });


    setInterval(function () {
        $.get(todayDiscountsUrl, null, function (data) {
            if (parseInt(data) > Number(todayDiscounts)) {
                var diff = parseInt(data) - Number(todayDiscounts);
                var productList = "";
                var $dialogMessage = $("#dialog-message");
                $.getJSON("http://api.2for1.pro/api/list?disfrom=50&Pz=" + diff + "&Pi=0&request_source=2&sort=uid", function (json) {
                    $dialogMessage.empty();
                    $.each(json, function (p, item) {
                        if (Number(item.Type) < 100) {
                            productList = productList + "<p>- " + item.Title + " with " + item.Type + "% discount" + "</p>";
                        }
                    });
                    if (productList.length > 0) {
                        $dialogMessage.append(productList);
                        dialog.dialog("open");
                    }

                });
            }
        }, "text");
    }, checkInterval);


    var dialog = $("#dialog-message").dialog({
        modal: true,
        autoOpen: false,
        width: 480,
        buttons: {
            "Yes, I want refresh page!": function () {
                location.reload();
            },
            Cancel: function () {
                dialog.dialog("close");
                $(document).scrollTop(0);
            }
        }
    });

    $body.on('click', '.button-google-play > a', function () {
        try {
            ga('send', 'event', 'app', 'go2app', 'gplay');
        } catch (e) {
        }
    });

    $body.on('click', '.button-go > a', function () {
        try {
            ga('send', 'event', 'ext_order', 'click2exit', $(this).attr('href'));
        } catch (e) {
        }
    });


    $("#button-previous").click(function () {
        try {
            ga('send', 'event', 'reload_page', 'prev', 'PREVIOUS PAGE');
        } catch (e) {
        }
    });


    $("#button-home").click(function () {
        try {
            ga('send', 'event', 'reload_page', 'home', 'HOME PAGE');
        } catch (e) {
        }
    });


    $("#button-next").click(function () {
        try {
            ga('send', 'event', 'reload_page', 'next', 'NEXT PAGE');
        } catch (e) {
        }
    });


    //window resize
    var currentWidth = 960;
    $(window).resize(function () {
        var winWidth = $(window).width();
        var conWidth;
        if (winWidth < 660) {
            conWidth = 440;
            col = 2
        } else if (winWidth < 880) {
            conWidth = 660;
            col = 3
        } else if (winWidth < 960) {
            conWidth = 880;
            col = 4;
        } else {
            conWidth = 960;
            col = 4;
        }

        if (conWidth != currentWidth) {
            currentWidth = conWidth;
            var $divProducts = $('#products');
            $divProducts.width(conWidth);
            $divProducts.BlocksIt({
                numOfCol: col,
                offsetX: 8,
                offsetY: 8
            });
        }
    });


    function getParam(sname) {
        var params = location.search.substr(location.search.indexOf("?") + 1);
        var sval = "";
        params = params.split("&");
        // split param and value into individual pieces
        for (var i = 0; i < params.length; i++) {
            temp = params[i].split("=");
            if ([temp[0]] == sname) {
                sval = temp[1];
            }
        }
        return sval;
    }
});