# 2for1 live (startup) #

## Summary: ##

50+ % fashion goods sales from the biggest online stores. There are more than 100,000 items in our database from famous US stores such as H&M, Macy’s, 6pm, DSW, Overstock, Nordstrom and many others. All with 50+ % discounts. We do not offer coupons and promo codes, we show the products with crazy discounts which on sales right now!

## Technical summary: ##

- worked with 2for1 API (API documentation http://2for1.slognosti.ru/api/, http://api.2for1.pro/api/user);

- use jQuery v1.7.1, jQuery UI library v1.11.2, jQuery plugin Lightbox v2.7.1, jQuery plugin BlocksIt v1.0 (http://www.inwebson.com/jquery/blocksit-js-dynamic-grid-layout-jquery-plugin/);

- Google analytics integrated;

- AddThis social share widget integrated;